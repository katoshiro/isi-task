#! /bin/bash

# set -x
echo "Starting PREP"

# install virtual enviroment with pip:
[[ ! -d ".venv" ]] && echo "Virtual environment does not exist. Creating it in .venv" && python3 -m venv .venv
echo -e "\n"

# Activating virtual environment
echo "Sourcing Python enviroment."
source .venv/bin/activate
echo -e "\n"

# install all other libraries from requirements.txt
echo "Installing python libraries (requrements.txt)..."
pip3 install -r requirements.txt
echo -e "\n"

# making sure we have firefox
firefox_installed="True"
[[ ! -f "/usr/bin/firefox" ]] && firefox_installed="False" 

# check if user is root
root_user="True"
echo "Checking if Firefox is installed..."
[[ $(id -u) != 0 ]] && root_user="False"

# get right firefox package for the distro(Ubuntu or Debian) in use.
firefox_pkg_name=""
distro=$(cat /etc/*rele* | grep "^ID=" | sed  "s/^ID=//")
if [[ $distro == 'debian' ]]; then
    firefox_pkg_name="firefox-esr"
elif [[ $distro == 'ubuntu' ]]; then
    firefox_pkg_name="firefox"
else
    echo "Distro is not Ubuntu nor Debian. Exiting" && exit 1
fi

# firefox not installed and not root -> exit
[[ $firefox_installed == "False" ]] && [[ $root_user == "False" ]] && echo "Firefox not found at '/usr/bin/firefox'. Please run the script as root or use sudo, or install firefox by yourself. Exiting" && exit 1

# install firefox when root
[[ $firefox_installed == "False" ]] && [[ $root_user == "True" ]] && echo "Installing Firefox as it was not found in '/usr/bin/firefox' and it is needed." && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -yq  install $firefox_pkg_name
echo -e "\n"

echo "END PREP"
echo -e "\n\n"

# Run the main function 
echo "Running main function - main.py:"
python3 main.py

# deactivating virtual environment
deactivate

# set +x