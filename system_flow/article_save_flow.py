from typing import Dict, List
import variables as var
from scraper.data_manager.title import normalize_title
from scraper.file_manager.save import save_json_to_file, file_exists
from scraper.page_traversers.article_traverser import get_article_body


def download_articles(articles: List[Dict[str, str]]) -> None:
    """Download each article in the articles if it's not downloaded"""

    for article in articles:
        print(f'Processing article "{article["title"]}"')

        # get unix friendly title
        new_article_title = normalize_title(article["title"])

        if file_exists(new_article_title):
            # Don't download if exists locally
            print(
                f'Not downloading. Already exists -> "{var.DOWNLOAD_FOLDER + new_article_title}"'
            )
            continue

        try:
            # get article 'clean' body
            article_body = get_article_body(article["url"])

            # standardize data to dict
            processed_article = {}
            processed_article["title"] = new_article_title
            processed_article["body"] = article_body

            # save data(article) as JSON locally
            save_json_to_file(processed_article)
        except Exception as error:
            print(
                "Unable to get article '"
                + str(article["title"])
                + "', with URL'"
                + str(article["url"])
                + f"'.\nError was: {error}"
            )

    print("All articles successfully downloaded")
