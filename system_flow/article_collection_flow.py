import variables as var
from typing import Dict, List
from scraper.driver import selenium_driver
from scraper.page_traversers.collector_class import ArticleCollector

### !!! setting variable UNTIL_PAGE in variables.py used by get_bottom_section(), to 2 for testing purposes.
### in "production", UNTIL_PAGE can be set to "None" or omitted, so get_bottom_section() can go over all pages.


def article_collector_bbc_logic(url: str) -> List[Dict[str, str]]:
    """get's articles for section like /bushiness, /technology, etc"""

    print(f"Collecting articles for URL - {url}")

    driver = selenium_driver()
    driver.get(url)
    article_collector = ArticleCollector()

    # add up the top, middle and bottom article data
    articles_top = article_collector.get_top_section(driver=driver)
    articles_middle = article_collector.get_middle_section(driver=driver)
    articles_bottom = article_collector.get_bottom_section(
        driver=driver, until_page=var.UNTIL_PAGE
    )  ### !!! setting until_page to UNTIL_PAGE for testing purposes. in "production" this until_page can be omitted
    driver.quit()

    all_articles = articles_top + articles_middle + articles_bottom

    print(f"DONE: Collecting articles for URL - {url}\n\n")
    return all_articles


def collect_tech_and_bussiness_bbc_articles() -> List[Dict[str, str]]:
    business_url = var.BBC_NEWS_URL + var.BUSINESS
    technology_url = var.BBC_NEWS_URL + var.TECHNOLOGY

    # add up BBC' business and technology articles
    business_articles = article_collector_bbc_logic(url=business_url)
    technology_articles = article_collector_bbc_logic(url=technology_url)

    total_articles = business_articles + technology_articles
    return total_articles
