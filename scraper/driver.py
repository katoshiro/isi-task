from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.support.ui import WebDriverWait


def selenium_driver() -> webdriver:
    """instantiate selenium driver, firefox browser and geckodriver"""

    service = Service(executable_path="/snap/bin/geckodriver")
    options = webdriver.FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(service=service, options=options)
    driver.implicitly_wait(15)  # wait for element to load before erroring right away

    return driver
