def normalize_title(title) -> str:
    """Converts the title to more Linux friendly string"""

    for char in [
        " ",
        '"',
        "'",
        ":",
    ]:
        title = title.replace(char, "_")

    title = title.lower()
    title = title.replace(",", "")
    title = title.replace("$", "USD_")
    title = title.replace("€", "EUR_")
    title = title.replace("£", "GBP_")

    return title
