import time
from scraper.driver import selenium_driver
from selenium.webdriver.common.by import By


def get_article_body(url: str) -> str:
    """fetch the plane text from the main body of the article."""

    print("fetching the body...")
    driver = selenium_driver()
    driver.get(url)

    # give time for all elements to load
    time.sleep(5)
    # get container of the mai  body
    container = driver.find_element(By.ID, "main-content")

    # get paragraphs and paragraph headlines inside the body
    paragraphs = container.find_elements(
        By.CSS_SELECTOR,
        ('div[data-component="text-block"], div[data-component="subheadline-block"]'),
    )

    article_body = ""

    # add them to a simple string, but divide them via new line
    for paragraph in paragraphs:
        article_body += paragraph.text + "\n"

    driver.quit()
    return article_body
