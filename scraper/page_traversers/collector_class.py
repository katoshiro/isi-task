import re
import time
from selenium import webdriver
from typing import Dict, List, Tuple
from abc import ABC, abstractmethod
from selenium.webdriver.common.by import By


class BBCArticleCollector(ABC):
    """Gather articles from webpage"""

    @abstractmethod
    def get_top_section(
        self, driver: webdriver, articles: Dict
    ) -> List[Dict[str, str]]:
        """Get TOP articles: headlines and URLs from top section"""
        pass

    @abstractmethod
    def get_middle_section(
        self, driver: webdriver, articles: Dict
    ) -> List[Dict[str, str]]:
        """Get Middle articles: headlines and URLs from middle section"""
        pass

    @abstractmethod
    def get_bottom_section(
        self, driver: webdriver, articles: Dict, until_page: int = float("inf")
    ) -> Dict[str, str]:
        """Get bottom articles(Latest Updates): headlines and URLs from bottom section until last page"""
        pass

    @abstractmethod
    def change_page(self, driver, until_page: int = None) -> Tuple[int, int]:
        """Change to next page in the bottom articles(Latest Updates) until the end or until the value of until_page inclusive.
        returns number of page and until_page(last one by default)"""
        pass


class ArticleCollector(BBCArticleCollector):
    def get_top_section(
        self, driver: webdriver, articles: List = None
    ) -> list[Dict[str, str]]:
        print("Getting top articles: headlines and URLs from top section ")

        if articles is None:
            articles = []

        # get the biggest top-left article
        big_top_article = driver.find_element(
            By.XPATH,
            '//*[@id="topos-component"]/div[4]/div/div[1]/div/div[1]/div/div[2]/div[1]/a',
        )
        title = big_top_article.text
        url = big_top_article.get_attribute("href")

        articles.append({"title": title, "url": url})

        # get the container for the 6 articles on the right
        top_right_container = driver.find_element(
            By.XPATH, '//*[@id="topos-component"]/div[4]/div/div[1]/div/div[3]/div'
        )
        # get the 6 articles on the right
        top_articles = top_right_container.find_elements(
            By.CLASS_NAME, "gel-layout__item"
        )

        for top_article in top_articles:
            # get title and link
            title = top_article.find_element(By.TAG_NAME, "a").text
            url = top_article.find_element(By.TAG_NAME, "a").get_attribute("href")

            # add dict data to list
            articles.append({"title": title, "url": url})

        print("Getting top articles: DONE\n")
        return articles

    def get_middle_section(
        self, driver: webdriver, articles: List = None
    ) -> List[Dict[str, str]]:
        print("Getting middle articles: headlines and URLs from middle section")

        if articles is None:
            articles = []

        # get middle articles container
        middle_container = driver.find_element(
            By.CSS_SELECTOR, "[data-test-curated-topic-five-slice]"
        )
        # get all url for all articles in the container
        middle_articles_a_tags = middle_container.find_elements(By.TAG_NAME, "a")
        for middle_article in middle_articles_a_tags:
            # get url
            url = middle_article.get_attribute("href")

            if not re.search(r"\d+$", url):
                # don't add urls that are not targeted articles. Get clean url - exclude comment references
                continue

            # get title
            title = middle_article.find_element(By.TAG_NAME, "h3").text

            # add dict data to list
            articles.append({"title": title, "url": url})

        print("Getting middle articles: DONE\n")
        return articles

    def get_bottom_section(
        self, driver: webdriver, articles: List = None, until_page: int = None
    ) -> List[Dict[str, str]]:
        print(
            "Getting bottom articles(Latest Updates): headlines and URLs from bottom section "
            + ("for all pages" if until_page == None else f"until page {until_page}."),
        )

        if articles is None:
            articles = []

        current_page = 1
        need_to_change_page = True

        while need_to_change_page:
            print(f"collecting articles on page {current_page}")
            # find the container with the articles
            latest_articles = driver.find_elements(By.TAG_NAME, "article")
            for element in latest_articles:
                # get title
                bottom_article = element.find_element(By.TAG_NAME, "h3")
                title = bottom_article.text

                # get url
                url = bottom_article.find_element(By.TAG_NAME, "a").get_attribute(
                    "href"
                )

                # add dict data to list
                articles.append({"title": title, "url": url})

            if current_page == until_page:
                need_to_change_page = False
            else:
                current_page, until_page = self.change_page(driver, until_page)
        print("Getting bottom articles(Latest Updates): DONE\n")
        return articles

    def change_page(self, driver, until_page: int = None) -> Tuple[int, int]:
        print("Changing to next page...")

        # get next page "button"
        next_page_button = driver.find_element(By.CSS_SELECTOR, 'a[rel="next"]')
        driver.execute_script("arguments[0].click();", next_page_button)

        # wait 5 seconds to give all elements time to load. Otherwise the current_page holds the old value.
        time.sleep(5)

        # get current page
        current_page = int(
            driver.find_element(
                By.CSS_SELECTOR,
                ".lx-pagination__page-number.qa-pagination-current-page-number",
            ).text
        )

        # get last page if nor presented
        if until_page is None:
            until_page = int(
                driver.find_element(
                    By.CSS_SELECTOR,
                    ".lx-pagination__page-number.qa-pagination-total-page-number",
                ).text
            )
        return current_page, until_page
