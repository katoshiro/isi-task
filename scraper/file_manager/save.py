import os
import json
from typing import Dict
import variables as var


def save_json_to_file(processed_article: Dict) -> bool:
    """Save article body and title to JSON file"""

    if not os.path.exists(var.DOWNLOAD_FOLDER):
        # Create folder if it doesn't exist
        os.mkdir(var.DOWNLOAD_FOLDER)

    file_name = processed_article["title"]
    file_path = var.DOWNLOAD_FOLDER + file_name + var.FILE_EXTENSION

    # "convert" dict to JSON data
    json_data = json.dumps(processed_article, indent=4)

    # Save JSON data to file
    print(f"Saving to: {file_path}")
    with open(file_path, "w") as outfile:
        outfile.write(json_data)

    return True


def file_exists(file_name) -> bool:
    """Check if file already exists"""

    file_path = var.DOWNLOAD_FOLDER + file_name + var.FILE_EXTENSION
    if os.path.isfile(file_path):
        return True
    return False
