#! /bin/bash

echo "Running the program in docker container"
docker compose -p app up

echo "Cleaning up: removing the image and the container"
docker rm $(docker ps -a -f status=exited | grep "app-article_downloader" | awk {'print $1'})
docker rmi $(docker images -a |grep "app-article_downloader" | awk {'print $3'})