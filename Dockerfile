# use latest python
FROM python:latest

# set the workdir to \app
workdir /app 

# copy all the files into the container's workdir
COPY . .

# run the prep and main app
CMD ./run.sh
#CMD bash -c "sleep 999"