from system_flow.article_collection_flow import collect_tech_and_bussiness_bbc_articles
from system_flow.article_save_flow import download_articles


def main():
    # Collect articles
    articles = collect_tech_and_bussiness_bbc_articles()

    # Download them in JSON, if they are not downloaded already
    download_articles(articles)


if __name__ == "__main__":
    main()
