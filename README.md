# Article downloader for BBC website(businesses and technology sections)

The program collects articles from the `https://www.bbc.com/news` website, sections `/business` (Business - without the `Features & Analysis`, `Watch/Listen` and `Special reports` subsections) and `/technology` (Tech - without the `Watch/Listen` and `Features & Analysis` subsections).

After that it saves every article, to directory `articles/` created in the project directory, as a separate JSON file. Each JSON file keeps only the article content (TITLE and BODY). All non-relevant content (external links, links to categories, CSS, scripts, multimedia content, etc.) are not included.

The program keeps track of the downloaded content. If run again, the code will download only articles that have not been downloaded already.


## Technology

* Linux - Debian or Ubuntu
* Python
* Python virtual environment - venv module
* Selenium
* Firefox, geckodriver
* Docker
* Docker Compose

## Installation/usage

### Requirements:
#### Docker run - `docker_run.sh`:
* Docker 24.0.2 +

#### Bash run - `run.sh`:
* Python 3.10.6+
* Firefox 113.0.2+ 
    * if firefox is not installed the `run.sh` installs it - it needs `sudo` or `root` user

### Docker:
1. Navigate to source code directory via `bash`.
2. run the file `docker_run.sh`:
```bash
./docker_run.sh
```
It will prepare docker image and run a container. In the container it will prepare python virtual environment, download the required libraries and and run the program.
After the program has finished the container is stopped and destroyed and the image is deleted.

### Linux:
1. Navigate to source code directory via `bash`.
2. run the file `run.sh`:
```bash
./run.sh
```
It will install latest firefox (if missing when run with sudo or as root user), prepare a python virtual environment, download the required libraries and and run the program.
